package ru.akvine.tasklist.rest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.tasklist.ApiBaseTest;
import ru.akvine.tasklist.rest.dto.client.ClientRequest;
import ru.akvine.tasklist.rest.dto.client.GetClientRequest;
import ru.akvine.tasklist.rest.dto.client.UpdateClientRequest;
import ru.akvine.tasklist.rest.dto.response.CommonErrorCodes;

import static ru.akvine.tasklist.common.RestMethods.*;
import static ru.akvine.tasklist.common.TestConstants.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class ClientControllerTest extends ApiBaseTest {

    @Test
    @DisplayName("Get client fail - no session")
    void getClient_fail_noSession() throws Exception {
        GetClientRequest request = new GetClientRequest()
                .setUuid(CLIENT_UUID_1);
        doGet(CLIENT_GET_ENDPOINT, request)
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.errorCode").value(CommonErrorCodes.NO_SESSION_ERROR));
    }

    @Test
    @DisplayName("Get client fail - client not found")
    void getClient_fail_notFound() throws Exception {
        GetClientRequest request = new GetClientRequest()
                .setUuid(CLIENT_UUID_1);
        doGet(CLIENT_GET_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.errorCode").value(CommonErrorCodes.CLIENT_NOT_FOUND_ERROR));
    }

    @Test
    @DisplayName("Get client fail - invalid email")
    void getClient_fail_invalidEmail() throws Exception {
        GetClientRequest request = new GetClientRequest()
                .setUuid(CLIENT_UUID_1);
        doGet(CLIENT_GET_ENDPOINT, request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.errorCode").value(CommonErrorCodes.Validation.EMAIL_INVALID_ERROR));
    }

    @Test
    @DisplayName("Get client success")
    void getClient_success() throws Exception {
        String sessionId = doAuth(CLIENT_EMAIL_1);

        GetClientRequest request = new GetClientRequest()
                .setUuid(CLIENT_UUID_1);
        doGet(CLIENT_GET_ENDPOINT, sessionId, request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"));
    }

    @Test
    @DisplayName("Update client fail - no session")
    void updateClient_fail_noSession() throws Exception {
        ClientRequest request = new UpdateClientRequest()
                .setEmail(CLIENT_EMAIL_1)
                .setFirstName(CLIENT_FIRST_NAME_1)
                .setSecondName(CLIENT_SECOND_NAME_2);
        doGet(CLIENT_GET_ENDPOINT, request)
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.errorCode").value(CommonErrorCodes.NO_SESSION_ERROR));
    }
}
