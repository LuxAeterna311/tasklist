package ru.akvine.tasklist.rest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.tasklist.ApiBaseTest;
import ru.akvine.tasklist.rest.dto.response.CommonErrorCodes;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.rest.dto.security.RegisterRequest;

import static ru.akvine.tasklist.common.RestMethods.*;
import static ru.akvine.tasklist.common.TestConstants.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
class SecurityControllerTest extends ApiBaseTest {
    @Test
    @DisplayName("Success logout client")
    void logout_success() throws Exception {
        doGet(SECURITY_LOGOUT_ENDPOINT, null)
                .andExpect(jsonPath("$.status").value("SUCCESS"));
    }

    @Test
    @DisplayName("Fail auth - client not found")
    void auth_fail_clientNotFound() throws Exception {
        AuthRequest authRequest = new AuthRequest()
                .setEmail("some new email")
                .setPassword("123");
        doGet(SECURITY_AUTH_ENDPOINT, authRequest)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status").value("FAIL"))
                .andExpect(jsonPath("$.code").value(CommonErrorCodes.CLIENT_NOT_FOUND_ERROR));
    }

    @Test
    @DisplayName("Success auth client")
    void auth_success() throws Exception {
        AuthRequest authRequest = new AuthRequest()
                .setEmail(CLIENT_EMAIL_1)
                .setPassword("123");
        doGet(SECURITY_AUTH_ENDPOINT, authRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"));
    }

    @Test
    @DisplayName("Success register client")
    void register_success() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest()
                .setEmail("test@email.com")
                .setFirstName("firstName")
                .setSecondName("secondName")
                .setPassword("123");
        doGet(SECURITY_REGISTER_ENDPOINT, registerRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andExpect(jsonPath("$.client.firstName").value("firstName"))
                .andExpect(jsonPath("$.client.secondName").value("secondName"))
                .andExpect(jsonPath("$.client.email").value("test@email.com"));
    }

}