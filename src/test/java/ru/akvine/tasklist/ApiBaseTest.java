package ru.akvine.tasklist;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.service.ClientService;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.akvine.tasklist.common.RestMethods.SECURITY_AUTH_ENDPOINT;

@ExtendWith(SpringExtension.class)
@TestPropertySource(properties = {
        "spring.jpa.hibernate.ddl-auto=update"
})
@SpringBootTest
@AutoConfigureMockMvc
public class ApiBaseTest {
    protected static final String VALID_PASSWORD = "123";
    protected static final String SESSION_COOKIE_NAME = "COOKIE";

    @Autowired
    protected MockMvc mvc;
    @Autowired
    protected ClientService clientService;

    public static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected ResultActions doPost(String link, Object request) throws Exception {
        return doPost(link, null, request);
    }

    protected ResultActions doPost(String link, String sessionId, Object request) throws Exception {
        MockHttpServletRequestBuilder postReq =
                post(link, request)
                        .contentType(MediaType.APPLICATION_JSON);

        if (request != null) {
            postReq.content(toJson(request));
        }
        if (StringUtils.isNotBlank(sessionId)) {
            postReq.cookie(new Cookie(SESSION_COOKIE_NAME, sessionId));
        }

        return mvc.perform(postReq);
    }

    protected ResultActions doGet(String link, Object request) throws Exception {
        return doGet(link, null, request);
    }

    protected ResultActions doGet(String link, String sessionId, Object request) throws Exception {
        MockHttpServletRequestBuilder getReq =
                get(link)
                        .contentType(MediaType.APPLICATION_JSON);

        if (request != null) {
            getReq.content(toJson(request));
        }
        if (StringUtils.isNotBlank(sessionId)) {
            getReq.cookie(new Cookie(SESSION_COOKIE_NAME, sessionId));
        }

        return mvc.perform(getReq);
    }

    protected ResultActions doPut(String link, Object request) throws Exception {
        return doPut(link, null, request);
    }

    protected ResultActions doPut(String link, String sessionId, Object request) throws Exception {
        MockHttpServletRequestBuilder putReq =
                put(link)
                        .contentType(MediaType.APPLICATION_JSON);

        if (request != null) {
            putReq.content(toJson(request));
        }
        if (StringUtils.isNotBlank(sessionId)) {
            putReq.cookie(new Cookie(SESSION_COOKIE_NAME, sessionId));
        }

        return mvc.perform(putReq);
    }

    protected ResultActions doDelete(String link, Object request) throws Exception {
        return doDelete(link, null, request);
    }

    protected ResultActions doDelete(String link, String sessionId, Object request) throws Exception {
        MockHttpServletRequestBuilder deleteReq =
                delete(link)
                        .contentType(MediaType.APPLICATION_JSON);

        if (request != null) {
            deleteReq.content(toJson(request));
        }
        if (StringUtils.isNotBlank(sessionId)) {
            deleteReq.cookie(new Cookie(SESSION_COOKIE_NAME, sessionId));
        }

        return mvc.perform(deleteReq);
    }

    protected String doAuth(String email) throws Exception {
        clientService.getByEmail(email);
        Cookie session = doAuth(email, VALID_PASSWORD)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("SUCCESS"))
                .andReturn().getResponse().getCookie(SESSION_COOKIE_NAME);
        Preconditions.checkNotNull(session);
        return session.getValue();
    }

    protected ResultActions doAuth(String email, String password) throws Exception {
        AuthRequest body = new AuthRequest()
                .setEmail(email)
                .setPassword(password);

        MockHttpServletRequestBuilder request =
                get(SECURITY_AUTH_ENDPOINT)
                        .content(toJson(body))
                        .contentType(MediaType.APPLICATION_JSON);
        return mvc.perform(request);
    }
}

