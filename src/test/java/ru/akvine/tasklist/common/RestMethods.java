package ru.akvine.tasklist.common;

public class RestMethods {

    // Security endpoints
    public static final String SECURITY_LOGOUT_ENDPOINT = "/security/logout";
    public static final String SECURITY_AUTH_ENDPOINT = "/security/auth";
    public static final String SECURITY_REGISTER_ENDPOINT = "/security/register";

    // Client endpoints
    public static final String CLIENT_GET_ENDPOINT = "/clients/get";
    public static final String CLIENT_UPDATE_ENDPOINT = "/clients/update";
}
