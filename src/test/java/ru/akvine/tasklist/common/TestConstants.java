package ru.akvine.tasklist.common;

public class TestConstants {
    // Clients
    public static final String CLIENT_EMAIL_1 = "test_1@email.com";
    public static final String CLIENT_UUID_1 = "client_uuid_1";
    public static final String CLIENT_FIRST_NAME_1 = "first_name_1";
    public static final String CLIENT_SECOND_NAME_2 = "second_name_2";
}
