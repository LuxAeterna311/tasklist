package ru.akvine.tasklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.akvine.tasklist.entity.TaskEntity;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
    @Query("from TaskEntity te where te.uuid = :uuid and te.deleted = false")
    Optional<TaskEntity> findByUuid(@Param("uuid") String uuid);

    @Query("from TaskEntity te where te.client.id = :clientId and te.deleted = false")
    List<TaskEntity> findByClientId(@Param("clientId") Long clientId);

    @Modifying
    @Query("update TaskEntity te set te.deleted = true, te.deletedDate = :deletedDate where te.client.id = :clientId and te.deleted = false")
    void deleteByClientId(@Param("clientId") Long clientId, @Param("deletedDate") Date deletedDate);
}
