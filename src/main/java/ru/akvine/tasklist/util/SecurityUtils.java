package ru.akvine.tasklist.util;

import com.google.common.base.Preconditions;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.akvine.tasklist.config.ClientAuthentication;
import ru.akvine.tasklist.service.dto.Client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SecurityUtils {
    public static HttpSession getSession(HttpServletRequest httpRequest) {
        HttpSession session = httpRequest.getSession(false);
        if (session == null) {
            throw new RuntimeException("No session!");
        }
        return session;
    }

    public static void authenticate(Client client) {
        SecurityContextHolder
                .getContext()
                .setAuthentication(
                        new ClientAuthentication(
                                client.getEmail(),
                                client.getId()
                        )
                );
    }

    public static ClientAuthentication getCurrentUser() {
        ClientAuthentication user = getCurrentUserOrNull();
        Preconditions.checkNotNull(user, "user is null");
        return user;
    }

    public static ClientAuthentication getCurrentUserOrNull() {
        Authentication authentication = SecurityContextHolder
                .getContext()
                .getAuthentication();

        if (authentication instanceof ClientAuthentication) {
            return (ClientAuthentication) authentication;
        }

        return null;
    }

    public static void doLogout(HttpServletRequest request) {
        if (request == null) {
            return;
        }

        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.invalidate();

        ClientAuthentication user = getCurrentUserOrNull();
        if (user == null) {
            return;
        }
        SecurityContextHolder.clearContext();
    }
}
