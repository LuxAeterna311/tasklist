package ru.akvine.tasklist.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.akvine.tasklist.exception.BadCredentialsException;
import ru.akvine.tasklist.exception.ClientNotFoundException;
import ru.akvine.tasklist.exception.EmailInvalidException;
import ru.akvine.tasklist.exception.TaskNotFoundException;
import ru.akvine.tasklist.rest.dto.response.CommonErrorCodes;
import ru.akvine.tasklist.rest.dto.response.ErrorResponse;

import java.util.Date;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({ClientNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleClientNotFoundException(ClientNotFoundException exception) {
        logger.error("Client not found error, {}", exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.CLIENT_NOT_FOUND_ERROR,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({TaskNotFoundException.class})
    public ResponseEntity<ErrorResponse> handleTaskNotFoundException(TaskNotFoundException exception) {
        logger.error("Task not found error, {}", exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.TASK_NOT_FOUND_ERROR,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({EmailInvalidException.class})
    public ResponseEntity<ErrorResponse> handleEmailInvalidException(EmailInvalidException exception) {
        logger.error("Email invalid error, {}", exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.Validation.EMAIL_INVALID_ERROR,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({BadCredentialsException.class})
    public ResponseEntity<ErrorResponse> handleBadCredentials(BadCredentialsException exception) {
        logger.error("Bad credentials error, {}", exception.getMessage());
        ErrorResponse errorResponse = new ErrorResponse(
                CommonErrorCodes.BAD_CREDENTIALS_ERROR,
                exception.getMessage(),
                new Date()
        );
        return new ResponseEntity<>(errorResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
