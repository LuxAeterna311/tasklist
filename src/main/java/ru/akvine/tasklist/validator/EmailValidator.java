package ru.akvine.tasklist.validator;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.exception.EmailInvalidException;

import java.util.regex.Pattern;

@Component
@Slf4j
public class EmailValidator implements Validator<String> {
    private final static String EMAIL_PATTERN = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";

    @Override
    public void validate(String email) {
        if (StringUtils.isBlank(email) || !Pattern.matches(EMAIL_PATTERN, email)) {
            logger.error("Invalid email!");
            throw new EmailInvalidException("Email=" + email + " is invalid!");
        }
    }
}
