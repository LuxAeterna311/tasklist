package ru.akvine.tasklist.validator;

public interface Validator<T> {
    void validate(T obj);
}
