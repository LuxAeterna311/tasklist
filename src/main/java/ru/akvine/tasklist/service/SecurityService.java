package ru.akvine.tasklist.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.akvine.tasklist.entity.ClientEntity;
import ru.akvine.tasklist.exception.BadCredentialsException;
import ru.akvine.tasklist.exception.ClientNotFoundException;
import ru.akvine.tasklist.repository.ClientRepository;
import ru.akvine.tasklist.service.dto.Auth;
import ru.akvine.tasklist.service.dto.Client;

@Service
@RequiredArgsConstructor
@Slf4j
public class SecurityService {
    private final ClientRepository clientRepository;
    private final PasswordEncoder passwordEncoder;

    public Client authorize(Auth auth) {
        Preconditions.checkNotNull(auth, "auth is null");
        logger.debug("Authorize client by auth request=[{}]", auth);

        ClientEntity clientEntity = clientRepository
                .findByEmail(auth.getEmail())
                .orElseThrow(() -> new ClientNotFoundException("Client with email=" + auth.getEmail() + " not found!"));

        if (!verifyPasswords(auth.getPassword(), clientEntity.getHash())) {
            logger.error("Bad credentials!");
            throw new BadCredentialsException("Bad credentials!");
        }

        return new Client(clientEntity);
    }

    public boolean verifyPasswords(String password, String bCryptPassword) {
        return passwordEncoder.matches(password, bCryptPassword);
    }
}
