package ru.akvine.tasklist.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.akvine.tasklist.entity.ClientEntity;
import ru.akvine.tasklist.entity.TaskEntity;
import ru.akvine.tasklist.exception.TaskNotFoundException;
import ru.akvine.tasklist.repository.TaskRepository;
import ru.akvine.tasklist.service.dto.Task;
import ru.akvine.tasklist.util.UuidGeneratorUtil;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TaskService {
    private final TaskRepository taskRepository;
    private final ClientService clientService;

    public Task add(Task task) {
        Preconditions.checkNotNull(task, "task is null");
        logger.debug("Add task by task request=[{}]", task);

        ClientEntity clientEntity = clientService
                .getEntityById(task.getClientId());

        TaskEntity taskEntity = new TaskEntity()
                .setUuid(UuidGeneratorUtil.generate("-"))
                .setTitle(task.getTitle())
                .setClient(clientEntity)
                .setDescription(task.getDescription())
                .setCreatedDate(new Date());

        taskRepository.save(taskEntity);
        return new Task(taskEntity);
    }

    public Task getById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Get task by id=[{}]", id);
        return new Task(getEntityById(id));
    }

    public TaskEntity getEntityById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Get task entity by id=[{}]", id);
        return taskRepository
                .findById(id)
                .orElseThrow(() -> new TaskNotFoundException("Task with id=" + id + " not found!"));
    }

    public Task getByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.debug("Get task by uuid=[{}]", uuid);
        return new Task(getEntityByUuid(uuid));
    }

    public TaskEntity getEntityByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.debug("Get task entity by uuid=[{}]", uuid);
        return taskRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new TaskNotFoundException("Task with uuid=" + uuid + " not found!"));
    }

    public List<Task> getListByClientId(Long id) {
        Preconditions.checkNotNull(id, "client id is null");
        logger.debug("Get tasks list by id=[{}]", id);
        return taskRepository
                .findByClientId(id)
                .stream()
                .map(Task::new)
                .collect(Collectors.toList());
    }

    public Task update(Task task) {
        Preconditions.checkNotNull(task, "task is null");
        logger.debug("Update task by task request=[{}]", task);
        TaskEntity taskEntityToUpdate = getEntityByUuid(task.getUuid())
                .setTitle(task.getTitle())
                .setDescription(task.getDescription())
                .setUpdatedDate(new Date());

        TaskEntity updatedTaskEntity = taskRepository.save(taskEntityToUpdate);
        return new Task(updatedTaskEntity);
    }

    public void deleteById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Delete task by id=[{}]", id);
        TaskEntity taskEntity = getEntityById(id)
                .setDeleted(true)
                .setDeletedDate(new Date());

        taskRepository.save(taskEntity);
    }

    public void deleteByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.debug("Delete task by uuid=[{}]", uuid);
        TaskEntity taskEntity = getEntityByUuid(uuid)
                .setDeleted(true)
                .setDeletedDate(new Date());

        taskRepository.save(taskEntity);
    }
}
