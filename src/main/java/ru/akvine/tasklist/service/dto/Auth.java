package ru.akvine.tasklist.service.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class Auth {
    private String email;
    private String password;
}
