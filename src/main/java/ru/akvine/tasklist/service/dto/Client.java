package ru.akvine.tasklist.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.tasklist.entity.ClientEntity;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class Client {
    private Long id;
    private String uuid;
    private String firstName;
    private String secondName;
    private String email;
    private Date createdDate;
    private Date updatedDate;
    private Date deletedDate;
    private boolean deleted;

    private String password;
    private String hash;

    public Client(ClientEntity clientEntity) {
        this.id = clientEntity.getId();
        this.uuid = clientEntity.getUuid();
        this.firstName = clientEntity.getFirstName();
        this.secondName = clientEntity.getSecondName();
        this.email = clientEntity.getEmail();
        this.hash = clientEntity.getHash();
        this.createdDate = clientEntity.getCreatedDate();
        this.updatedDate = clientEntity.getUpdatedDate();
        this.deletedDate = clientEntity.getDeletedDate();
        this.deleted = clientEntity.isDeleted();
    }
}
