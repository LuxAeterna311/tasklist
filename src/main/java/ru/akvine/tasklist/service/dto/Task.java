package ru.akvine.tasklist.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.tasklist.entity.TaskEntity;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class Task {
    private Long id;
    private Long clientId;
    private String uuid;
    private String title;
    private String description;
    private Date createdDate;
    private Date updatedDate;
    private Date deletedDate;
    private boolean deleted;

    public Task(TaskEntity taskEntity) {
        this.id = taskEntity.getId();
        this.uuid = taskEntity.getUuid();
        this.title = taskEntity.getTitle();
        this.description = taskEntity.getDescription();
        this.clientId = taskEntity.getClient().getId();
        this.createdDate = taskEntity.getCreatedDate();
        this.updatedDate = taskEntity.getUpdatedDate();
        this.deletedDate = taskEntity.getDeletedDate();
        this.deleted = taskEntity.isDeleted();
    }
}
