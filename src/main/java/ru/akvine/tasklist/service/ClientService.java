package ru.akvine.tasklist.service;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.akvine.tasklist.entity.ClientEntity;
import ru.akvine.tasklist.exception.ClientNotFoundException;
import ru.akvine.tasklist.repository.ClientRepository;
import ru.akvine.tasklist.repository.TaskRepository;
import ru.akvine.tasklist.service.dto.Client;
import ru.akvine.tasklist.util.UuidGeneratorUtil;

import java.util.Date;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService {
    private final ClientRepository clientRepository;
    private final TaskRepository taskRepository;
    private final PasswordEncoder passwordEncoder;

    public Client add(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        Preconditions.checkNotNull(client.getEmail(), "client.email is null");
        logger.debug("Add client by client request=[{}]", client);

        ClientEntity clientEntity = new ClientEntity()
                .setUuid(UuidGeneratorUtil.generate("-"))
                .setFirstName(client.getFirstName())
                .setSecondName(client.getSecondName())
                .setEmail(client.getEmail())
                .setHash(passwordEncoder.encode(client.getPassword()))
                .setCreatedDate(new Date());

        ClientEntity savedClient = clientRepository.save(clientEntity);
        return new Client(savedClient);
    }

    public Client getById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Get client by id=[{}]", id);
        return new Client(getEntityById(id));
    }

    public ClientEntity getEntityById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Get client entity by id=[{}]", id);
        return clientRepository
                .findById(id)
                .orElseThrow(() -> new ClientNotFoundException("Client with id=" + id + " not found"));
    }

    public Client getByEmail(String email) {
        Preconditions.checkNotNull(email, "email is null");
        logger.debug("Get client by email=[{}]", email);
        return clientRepository
                .findByEmail(email)
                .map(Client::new)
                .orElseThrow(() -> new ClientNotFoundException("Client with email=" + email + " not found"));
    }

    public Client getByUuid(String uuid) {
        Preconditions.checkNotNull(uuid);
        logger.debug("Get client by uuid=[{}]", uuid);
        return new Client(getEntityByUuid(uuid));
    }

    public ClientEntity getEntityByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.debug("Get client entity by uuid=[{}]", uuid);
        return clientRepository
                .findByUuid(uuid)
                .orElseThrow(() -> new ClientNotFoundException("Client with uuid=" + uuid + " not found"));
    }

    public Client update(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        Preconditions.checkNotNull(client.getUuid(), "client.uuid is null");
        logger.debug("Update client by client request=[{}]", client);

        ClientEntity clientEntityToUpdate = getEntityByUuid(client.getUuid())
                .setUpdatedDate(new Date())
                .setFirstName(client.getFirstName())
                .setSecondName(client.getSecondName())
                .setEmail(client.getEmail());

        ClientEntity updatedClientEntity = clientRepository.save(clientEntityToUpdate);
        return new Client(updatedClientEntity);
    }

    @Transactional
    public void deleteById(Long id) {
        Preconditions.checkNotNull(id, "id is null");
        logger.debug("Delete client by id=[{}]", id);

        ClientEntity clientEntity = getEntityById(id)
                .setDeleted(true)
                .setDeletedDate(new Date());

        taskRepository.deleteByClientId(clientEntity.getId(), new Date());
        clientRepository.save(clientEntity);
    }

    @Transactional
    public void deleteByUuid(String uuid) {
        Preconditions.checkNotNull(uuid, "uuid is null");
        logger.debug("Delete client by uuid=[{}]", uuid);

        ClientEntity clientEntity = getEntityByUuid(uuid)
                .setDeleted(true)
                .setDeletedDate(new Date());

        taskRepository.deleteByClientId(clientEntity.getId(), new Date());
        clientRepository.save(clientEntity);
    }
}
