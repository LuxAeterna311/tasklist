package ru.akvine.tasklist.converter;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.rest.dto.client.ClientDto;
import ru.akvine.tasklist.rest.dto.client.ClientRequest;
import ru.akvine.tasklist.rest.dto.client.ClientResponse;
import ru.akvine.tasklist.service.dto.Client;

@Component
public class ClientConverter {
    public Client convertToClient(ClientRequest clientRequest) {
        Preconditions.checkNotNull(clientRequest, "clientRequest is null");
        return new Client()
                .setFirstName(clientRequest.getFirstName())
                .setSecondName(clientRequest.getSecondName())
                .setEmail(clientRequest.getEmail());
    }

    public ClientResponse convertToClientResponse(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        return new ClientResponse()
                .setClient(
                        new ClientDto()
                        .setUuid(client.getUuid())
                        .setFirstName(client.getFirstName())
                        .setSecondName(client.getSecondName())
                        .setEmail(client.getEmail())
                );
    }
}
