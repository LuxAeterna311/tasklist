package ru.akvine.tasklist.converter;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.rest.dto.task.TaskDto;
import ru.akvine.tasklist.rest.dto.task.TaskListResponse;
import ru.akvine.tasklist.rest.dto.task.TaskRequest;
import ru.akvine.tasklist.rest.dto.task.TaskResponse;
import ru.akvine.tasklist.service.dto.Task;
import ru.akvine.tasklist.util.SecurityUtils;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TaskConverter {
    public Task convertToTask(TaskRequest taskRequest) {
        Preconditions.checkNotNull(taskRequest, "taskRequest is null");
        return new Task()
                .setTitle(taskRequest.getTitle())
                .setClientId(SecurityUtils.getCurrentUserOrNull().clientId())
                .setDescription(taskRequest.getDescription());
    }

    public TaskResponse convertToTaskResponse(Task task) {
        Preconditions.checkNotNull(task, "task is null");
        return new TaskResponse()
                .setTask(buildTaskDto(task));
    }

    public TaskListResponse convertToTaskListResponse(List<Task> tasks) {
        Preconditions.checkNotNull(tasks, "tasks is null");
        List<TaskDto> taskDtos = tasks
                .stream()
                .map(this::buildTaskDto)
                .collect(Collectors.toList());
        return new TaskListResponse()
                .setTasks(taskDtos);
    }

    private TaskDto buildTaskDto(Task task) {
        return new TaskDto()
                .setUuid(task.getUuid())
                .setDescription(task.getDescription())
                .setTitle(task.getTitle())
                .setCreatedDate(task.getCreatedDate())
                .setUpdatedDate(task.getUpdatedDate());
    }
}
