package ru.akvine.tasklist.converter;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.rest.dto.client.ClientDto;
import ru.akvine.tasklist.rest.dto.client.ClientResponse;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.rest.dto.security.RegisterRequest;
import ru.akvine.tasklist.service.dto.Auth;
import ru.akvine.tasklist.service.dto.Client;

@Component
public class SecurityConverter {
    public Client convertToClient(RegisterRequest request) {
        Preconditions.checkNotNull(request, "registerRequest is null");
        return new Client()
                .setFirstName(request.getFirstName())
                .setSecondName(request.getSecondName())
                .setEmail(request.getEmail())
                .setPassword(request.getPassword());
    }

    public Auth convertToAuth(AuthRequest request) {
        Preconditions.checkNotNull(request, "authRequest is null");
        return new Auth()
                .setEmail(request.getEmail())
                .setPassword(request.getPassword());
    }

    public ClientResponse convertToClientResponse(Client client) {
        Preconditions.checkNotNull(client, "client is null");
        return new ClientResponse()
                .setClient(
                        new ClientDto()
                                .setUuid(client.getUuid())
                                .setFirstName(client.getFirstName())
                                .setSecondName(client.getSecondName())
                                .setEmail(client.getEmail())
                );
    }
}
