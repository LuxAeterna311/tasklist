package ru.akvine.tasklist.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CLIENT")
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class ClientEntity {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CLIENT_SEQUENCE")
    @SequenceGenerator(name = "CLIENT_SEQUENCE", sequenceName = "CLIENT_ID_SEQUENCE", allocationSize = 1000)
    private Long id;

    @Column(name = "UUID", unique = true, nullable = false)
    private String uuid;

    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    @Column(name = "SECOND_NAME", nullable = false)
    private String secondName;

    @Column(name = "EMAIL", nullable = false)
    private String email;

    @Column(name = "HASH", nullable = false)
    private String hash;

    @Column(name = "CREATED_DATE", nullable = false)
    private Date createdDate;

    @Column(name = "UPDATED_DATE")
    private Date updatedDate;

    @Column(name = "DELETED_DATE")
    private Date deletedDate;

    @Column(name = "IS_DELETED", nullable = false)
    private boolean deleted;
}
