package ru.akvine.tasklist.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.tasklist.converter.ClientConverter;
import ru.akvine.tasklist.rest.dto.client.DeleteClientRequest;
import ru.akvine.tasklist.rest.dto.client.GetClientRequest;
import ru.akvine.tasklist.rest.dto.client.UpdateClientRequest;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;
import ru.akvine.tasklist.rest.meta.ClientControllerMeta;
import ru.akvine.tasklist.rest.validator.ClientValidator;
import ru.akvine.tasklist.service.ClientService;
import ru.akvine.tasklist.service.dto.Client;
import ru.akvine.tasklist.util.SecurityUtils;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@Tag(name = "Client API")
public class ClientController implements ClientControllerMeta {
    private final ClientService clientService;
    private final ClientConverter clientConverter;
    private final ClientValidator clientValidator;

    @Override
    public Response get(@RequestBody GetClientRequest request) {
        Client client = clientService.getByUuid(request.getUuid());
        return clientConverter.convertToClientResponse(client);
    }

    @Override
    public Response update(@RequestBody UpdateClientRequest request) {
        clientValidator.verifyClientRequest(request);
        Client client = clientConverter.convertToClient(request);
        Client updateClient = clientService.update(client);
        return clientConverter.convertToClientResponse(updateClient);
    }

    @Override
    public Response delete(@RequestBody DeleteClientRequest request, HttpServletRequest httpServletRequest) {
        clientService.deleteByUuid(request.getUuid());
        SecurityUtils.doLogout(httpServletRequest);
        return new SuccessfulResponse();
    }
}
