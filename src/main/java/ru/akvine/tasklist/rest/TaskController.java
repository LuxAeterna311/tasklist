package ru.akvine.tasklist.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.tasklist.converter.TaskConverter;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;
import ru.akvine.tasklist.rest.dto.task.AddTaskRequest;
import ru.akvine.tasklist.rest.dto.task.DeleteTaskRequest;
import ru.akvine.tasklist.rest.dto.task.GetTaskRequest;
import ru.akvine.tasklist.rest.dto.task.UpdateTaskRequest;
import ru.akvine.tasklist.rest.meta.TaskControllerMeta;
import ru.akvine.tasklist.service.TaskService;
import ru.akvine.tasklist.service.dto.Task;
import ru.akvine.tasklist.util.SecurityUtils;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "Task API")
public class TaskController implements TaskControllerMeta {
    private final TaskService taskService;
    private final TaskConverter taskConverter;

    @Override
    public Response get(@RequestBody GetTaskRequest request) {
        Task task = taskService.getByUuid(request.getUuid());
        return taskConverter.convertToTaskResponse(task);
    }

    @Override
    public Response list() {
        List<Task> tasks = taskService.getListByClientId(SecurityUtils.getCurrentUser().clientId());
        return taskConverter.convertToTaskListResponse(tasks);
    }

    @Override
    public Response add(@RequestBody AddTaskRequest request) {
        Task task = taskConverter.convertToTask(request);
        Task addTask = taskService.add(task);
        return taskConverter.convertToTaskResponse(addTask);
    }

    @Override
    public Response update(@RequestBody UpdateTaskRequest request) {
        Task task = taskConverter.convertToTask(request);
        Task updateTask = taskService.update(task);
        return taskConverter.convertToTaskResponse(updateTask);
    }

    @Override
    public Response delete(@RequestBody DeleteTaskRequest request) {
        taskService.deleteByUuid(request.getUuid());
        return new SuccessfulResponse();
    }
}
