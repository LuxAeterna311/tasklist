package ru.akvine.tasklist.rest.validator;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.rest.dto.security.RegisterRequest;
import ru.akvine.tasklist.validator.EmailValidator;

@Component
@RequiredArgsConstructor
public class SecurityValidator {
    private final EmailValidator emailValidator;

    public void verifyRegisterRequest(RegisterRequest request) {
        Preconditions.checkNotNull(request, "registerRequest is null");
        emailValidator.validate(request.getEmail());
    }

    public void verifyAuthRequest(AuthRequest request) {
        Preconditions.checkNotNull(request, "authRequest is null");
        emailValidator.validate(request.getEmail());
    }
}
