package ru.akvine.tasklist.rest.validator;

import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.akvine.tasklist.rest.dto.client.ClientRequest;
import ru.akvine.tasklist.validator.EmailValidator;

@Component
@RequiredArgsConstructor
public class ClientValidator {
    private final EmailValidator emailValidator;

    public void verifyClientRequest(ClientRequest clientRequest) {
        Preconditions.checkNotNull(clientRequest, "clientRequest is null");
        emailValidator.validate(clientRequest.getEmail());
    }
}
