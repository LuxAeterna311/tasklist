package ru.akvine.tasklist.rest.dto.response;

public class CommonErrorCodes {
    public static final String CLIENT_NOT_FOUND_ERROR = "client.notFound.error";
    public static final String TASK_NOT_FOUND_ERROR = "task.notFound.error";

    public static final String NO_SESSION_ERROR = "no.session.error";
    public static final String BAD_CREDENTIALS_ERROR = "bad.credentials.error";

    public interface Validation {
        String EMAIL_INVALID_ERROR = "email.invalid.error";
    }
}
