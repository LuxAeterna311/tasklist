package ru.akvine.tasklist.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;

@Getter
@Setter
@Accessors(chain = true)
public class ClientResponse extends SuccessfulResponse {
    private ClientDto client;
}
