package ru.akvine.tasklist.rest.dto.response;

import lombok.Getter;

import java.util.Date;

@Getter
public class ErrorResponse implements Response {
    private final ResponseStatus status = ResponseStatus.FAIL;
    private final String errorCode;
    private final String message;
    private Date time;

    public ErrorResponse(String errorCode, String message, Date time) {
        this.errorCode = errorCode;
        this.message = message;
        this.time = time;
    }

    public ErrorResponse(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
}


