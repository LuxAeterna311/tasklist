package ru.akvine.tasklist.rest.dto.task;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;

@Getter
@Setter
@Accessors(chain = true)
public class TaskResponse extends SuccessfulResponse {
    private TaskDto task;
}
