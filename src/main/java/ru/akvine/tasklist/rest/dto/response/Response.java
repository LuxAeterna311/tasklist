package ru.akvine.tasklist.rest.dto.response;

public interface Response {
    ResponseStatus getStatus();
}
