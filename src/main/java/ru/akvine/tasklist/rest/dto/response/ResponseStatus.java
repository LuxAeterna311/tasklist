package ru.akvine.tasklist.rest.dto.response;

public enum ResponseStatus {
    SUCCESS,
    FAIL
}
