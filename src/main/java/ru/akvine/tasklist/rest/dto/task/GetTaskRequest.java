package ru.akvine.tasklist.rest.dto.task;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class GetTaskRequest {
    private String uuid;
}
