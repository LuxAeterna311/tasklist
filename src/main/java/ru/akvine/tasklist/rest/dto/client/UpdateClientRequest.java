package ru.akvine.tasklist.rest.dto.client;

import lombok.experimental.Accessors;

@Accessors(chain = true)
public class UpdateClientRequest extends ClientRequest {
}
