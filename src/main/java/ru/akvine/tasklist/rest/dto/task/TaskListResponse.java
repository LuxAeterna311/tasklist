package ru.akvine.tasklist.rest.dto.task;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;

import java.util.List;

@Data
@Accessors(chain = true)
public class TaskListResponse extends SuccessfulResponse {
    private List<TaskDto> tasks;
}
