package ru.akvine.tasklist.rest.dto.client;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DeleteClientRequest {
    private String uuid;
}
