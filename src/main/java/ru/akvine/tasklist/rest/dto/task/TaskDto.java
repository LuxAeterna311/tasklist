package ru.akvine.tasklist.rest.dto.task;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class TaskDto {
    private String uuid;
    private String title;
    private String description;
    private Date createdDate;
    private Date updatedDate;
}
