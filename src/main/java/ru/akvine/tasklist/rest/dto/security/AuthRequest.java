package ru.akvine.tasklist.rest.dto.security;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class AuthRequest {
    private String email;
    private String password;
}
