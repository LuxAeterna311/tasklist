package ru.akvine.tasklist.rest.meta;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;
import ru.akvine.tasklist.rest.dto.client.ClientResponse;
import ru.akvine.tasklist.rest.dto.client.DeleteClientRequest;
import ru.akvine.tasklist.rest.dto.client.GetClientRequest;
import ru.akvine.tasklist.rest.dto.client.UpdateClientRequest;
import ru.akvine.tasklist.rest.dto.response.ErrorResponse;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/clients")
public interface ClientControllerMeta {
    @Operation(summary = "Get client by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping
    Response get(@RequestBody GetClientRequest request);

    @Operation(summary = "Update client by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @PutMapping
    Response update(@RequestBody UpdateClientRequest request);

    @Operation(summary = "Delete client by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SuccessfulResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @DeleteMapping
    Response delete(@RequestBody DeleteClientRequest request, HttpServletRequest httpServletRequest);
}
