package ru.akvine.tasklist.rest.meta;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.akvine.tasklist.rest.dto.client.ClientResponse;
import ru.akvine.tasklist.rest.dto.response.ErrorResponse;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.rest.dto.security.RegisterRequest;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/security")
public interface SecurityControllerMeta {

    @Operation(summary = "Register client endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Register client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ClientResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping("/register")
    Response register(@RequestBody RegisterRequest request);

    @Operation(summary = "Authentication client endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Authorize client",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SuccessfulResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping("/auth")
    Response auth(@RequestBody AuthRequest request);

    @Operation(summary = "Logout endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Current client logout",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SuccessfulResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping("/logout")
    Response logout(HttpServletRequest httpServletRequest);
}
