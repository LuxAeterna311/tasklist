package ru.akvine.tasklist.rest.meta;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;
import ru.akvine.tasklist.rest.dto.response.ErrorResponse;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;
import ru.akvine.tasklist.rest.dto.task.*;

@RequestMapping("/tasks")
public interface TaskControllerMeta {
    @Operation(summary = "Get tasks list")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get list",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskListResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping
    Response list();

    @Operation(summary = "Get task by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get task",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskListResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @GetMapping("/get")
    Response get(@RequestBody GetTaskRequest request);

    @Operation(summary = "Add task")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Add task",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskListResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @PostMapping
    Response add(@RequestBody AddTaskRequest request);

    @Operation(summary = "Update task by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Update task",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = TaskListResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @PutMapping
    Response update(@RequestBody UpdateTaskRequest request);

    @Operation(summary = "Delete task by uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete task",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = SuccessfulResponse.class))),
            @ApiResponse(responseCode = "400",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "404", description = "Can't find endpoint",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorResponse.class)))
    })
    @DeleteMapping
    Response delete(@RequestBody DeleteTaskRequest request);
}
