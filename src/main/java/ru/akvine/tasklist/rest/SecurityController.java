package ru.akvine.tasklist.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akvine.tasklist.converter.SecurityConverter;
import ru.akvine.tasklist.rest.dto.response.Response;
import ru.akvine.tasklist.rest.dto.response.SuccessfulResponse;
import ru.akvine.tasklist.rest.dto.security.AuthRequest;
import ru.akvine.tasklist.rest.dto.security.RegisterRequest;
import ru.akvine.tasklist.rest.meta.SecurityControllerMeta;
import ru.akvine.tasklist.rest.validator.SecurityValidator;
import ru.akvine.tasklist.service.ClientService;
import ru.akvine.tasklist.service.SecurityService;
import ru.akvine.tasklist.service.dto.Auth;
import ru.akvine.tasklist.service.dto.Client;
import ru.akvine.tasklist.util.SecurityUtils;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@Tag(name = "Security API")
public class SecurityController implements SecurityControllerMeta {
    private final ClientService clientService;
    private final SecurityService securityService;
    private final SecurityValidator securityValidator;
    private final SecurityConverter securityConverter;

    @Override
    public Response register(@RequestBody RegisterRequest request) {
        securityValidator.verifyRegisterRequest(request);
        Client client = securityConverter.convertToClient(request);
        Client registerClient = clientService.add(client);
        SecurityUtils.authenticate(registerClient);
        return securityConverter.convertToClientResponse(registerClient);
    }

    @Override
    public Response auth(@RequestBody AuthRequest request) {
        securityValidator.verifyAuthRequest(request);
        Auth auth = securityConverter.convertToAuth(request);
        Client client = securityService.authorize(auth);
        SecurityUtils.authenticate(client);
        return new SuccessfulResponse();
    }

    @Override
    public Response logout(HttpServletRequest httpServletRequest) {
        SecurityUtils.doLogout(httpServletRequest);
        return new SuccessfulResponse();
    }
}
