INSERT INTO CLIENT (ID, UUID, FIRST_NAME, SECOND_NAME, HASH, EMAIL, UPDATED_DATE, CREATED_DATE, DELETED_DATE, IS_DELETED)
VALUES (1, 'client_uuid_1', 'client_first_name_1', 'client_second_name_1', '$2a$12$gAgTcE0QaHmHo1O0aCvyK.51H7zqM9Bm3vXOz0iN5bhwoG3MLdeom', 'test_1@email.com', null, '2022-05-02 09:17:40.431', null, 0);
